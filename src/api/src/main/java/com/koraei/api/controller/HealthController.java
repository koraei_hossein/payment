package com.koraei.api.controller;

import com.koraei.core.repository.entity.DBHealthRepository;
import com.koraei.core.repository.memory.MonitoringRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@ApiIgnore
public class HealthController {


  @Autowired
  private MonitoringRepository monitoringRepository;

  @Autowired
  private DBHealthRepository dbHealthRepository;


  @RequestMapping(value = "/health", method = RequestMethod.GET, produces = "text/plain")
  public String monitor() {

    int db_check = 0;
    int redis_check = 0;
    int app_check = 1;

    StringBuilder builder = new StringBuilder();

    try {
      redis_check = monitoringRepository.check() ? 1 : 0;
    } catch (Exception ex) {
    }

//    try {
//      db_check = dbHealthRepository.check() > 0 ? 1 : 0;
//    } catch (Exception ex) {
//    }

    String result =
        String.format(
            "app %d \n" + "db %d \n" + "redis %d \n",
            app_check, db_check, redis_check);

    builder.append(result);

    return builder.toString();
  }
}
