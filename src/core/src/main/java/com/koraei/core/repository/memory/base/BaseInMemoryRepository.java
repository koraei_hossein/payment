package com.koraei.core.repository.memory.base;

public interface BaseInMemoryRepository {

  String getKey();
}
